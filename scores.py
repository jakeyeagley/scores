#!/usr/bin/python

#Rule 1: take predicted score 1 and subtract it from actual score 1 and add it to total
#Rule 2: take predicted score 2 and subtract it from actual score 2 and add it to total
#Rule 3: If Rule 1 equals 0 add 5 points or 10 points to person's total (5 for non-conference 10 for conference)
#Rule 4: If Rule 2 equals 0 add 5 points or 10 points to person's total (5 for non-conference 10 for conference)
#Rule 5: If team is correct add 5 points to total

import csv
import os
import sys
import json
from subprocess import Popen, PIPE

def calculateAverage(avg):
    return sum(avg) / len(avg)

def calculateScore(p1, p2, wt, a1, a2, awt, conf):
    p1 = int(p1)
    p2 = int(p2)
    a1 = int(a1)
    a2 = int(a2)

    total = 0

    if wt == awt:
        total = 5
    else:
        temp = p1
        p1 = p2
        p2 = temp

    d1 = abs(p1 - a1)
    d2 = abs(p2 - a2)

    if d1 == 0:
        if conf == True:
            total = total + 10
        else:
            total = total + 5

    if d2 == 0:
        if conf == True:
            total = total + 10
        else:
            total = total + 5

    total = total - d1 - d2

    return total

if len(sys.argv) > 1:
    file = os.path.expanduser(sys.argv[1])
else:
    file = os.path.expanduser('~/Cis-Com/scores/Basketball predictions 2019-2020.csv')

#if not os.path.isfile(file):
    #process = Popen(['scores.sh'], stdout=PIPE)
    #stdout, stderr = process.communicate()
    #print stdout
    #print stderr
    #if not os.path.isfile(file):
       #print "Error creating csv file. Make sure spreadsheet is open in numbers when running program"
        #sys.exit()


row_cnt = 0
players = []
empty_scores = dict()
last_col_name = ''
actuals = {}
# Import data from file
with open(file) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for row in csv_reader:
        row_cnt = row_cnt + 1
        col_cnt = 0
        game = ''
        empty_scores[row_cnt] = []

        for cell in row:
            col_cnt = col_cnt + 1
            if cell != '' and row_cnt == 1:
                players.append({'column' : col_cnt, 'player' : cell.strip(), 'Games' : [], 'total' : 0})

            if cell != '' and row_cnt > 1 and col_cnt == 1:
                game = cell

            if game != '' and cell == '' and col_cnt > 1:
                if row_cnt in empty_scores:
                    empty_scores[row_cnt].append(col_cnt)
                else:
                    empty_scores[row_cnt] = [col_cnt]

            if game != '' and col_cnt > 1 and cell == '':
                if players[col_cnt - 2]['player'] != 'Actual Final':
                    players[col_cnt - 2]['Games'].append({game : {'ws' : 0, 'ls' : 0, 'wt' : 'N/A', 'gameTotal' : 0}})
                    print "N/A"
                    print players[col_cnt - 2]['Games']

            if game != '' and col_cnt > 1 and cell != '':
                if cell.find('AVERAGE') == -1:
                    split1 = cell.split('-')
                    score1 = split1[0].strip()
                    split2 = split1[1].split('(')
                    score2 = split2[0].strip()
                    split3 = split2[1].split(')')
                    winner = split3[0].strip()
                    if players[col_cnt - 2]['player'] != 'Actual Final':
                        players[col_cnt - 2]['Games'].append({game : {'ws' : score1, 'ls' : score2, 'wt' : winner, 'gameTotal' : 0}})
                    else:
                        if split3[1].strip() == '*':
                            actuals[game] = {'ws' : score1, 'ls' : score2, 'wt' : winner, 'conf' : True, 'gameTotal' : 0}
                        else:
                            actuals[game] = {'ws' : score1, 'ls' : score2, 'wt' : winner, 'conf' : False, 'gameTotal' : 0}

#print '---actuals---'
#print actuals
row_scores = []
scores   = dict()
averages = dict()
for player in players: # iterate over each player
    if player['player'] == 'Actual Final':
        continue
    for game in player['Games']: # iterate over each game
        g = game.keys()[0]

        if game[g]['wt'] == 'N/A':
            continue
        gameScore = calculateScore(game[g]['ws'], game[g]['ls'], game[g]['wt'], actuals[g]['ws'], actuals[g]['ls'], actuals[g]['wt'], actuals[g]['conf'])
        game[g]['gameTotal'] = gameScore
        player['total'] = player['total'] + gameScore
        gameName = game.keys()[0]
        if gameName in scores:
            scores[gameName].append(gameScore)
        else:
            scores[gameName] = [gameScore]
# calculate average of game
for name in scores:
    avg = calculateAverage(scores[name])
    averages[name] = avg

# add averages to players that missed a game
game_cnt = len(averages)
for player in players:
    for playerGames in player['Games']:
        for playerGame in playerGames:
            if playerGames[playerGame]['wt'] == 'N/A':
                player['total'] = player['total'] + averages[playerGame]
                playerGames[playerGame]['gameTotal'] = averages[playerGame]

#print str(averages)
#print '--------------'
#print '------players-----'
#print str(players)

#with open('results.csv', 'w', newline='') as file:
    #writer = csv.writer(file)
    #rows = []
    #name_row = ['']
    #for player in players:
        #if player['player'] != 'Actual Final':
            #name_row.append(player['player'])
            #print str(player['player']) + " : " + str(player['total'])
            #row = []
            #for gameInfo in player['Games']:
                #for game, info in gameInfo.iteritems():
                    #row.append(str(info['gameTotal']))
                    #print str(game) + " : " + str(info['gameTotal'])
#
        #rows.append(row)
        #writer.writerow()
for player in players:
    if player['player'] != 'Actual Final':
        print '------' + str(player['player']) + " : " + str(player['total']) + '------'
        for gameInfo in player['Games']:
            for game, info in gameInfo.iteritems():
                print str(game) + " : " + str(info['gameTotal'])

print '--------Averages------------'
for game, avg in averages.iteritems():
    print str(game) + " average : " + str(avg)

for player in players:
    if player['player'] != 'Actual Final':
        print player['player'] + ":" + str(player['total'])
    #print "The avg for " + str(player) + " was " + str(avg)
#os.remove(file)
